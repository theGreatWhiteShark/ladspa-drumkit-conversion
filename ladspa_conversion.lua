--- Copy an existing Hydrogen drumkit and apply a number of LADSPA
--- effects to the samples of specific instruments to alter its sound.

posix = require( 'posix' )

-- This function will copy a file.
copyFile = function( oldFile, newFile )
   handleFileOld = io.open( oldFile, 'r' )
   handleFileNew = io.open( newFile, 'w+' )
   handleFileNew:write( handleFileOld:read( 'a' ) )
   handleFileOld:close()
   handleFileNew:close()
end

oldDrumkit = 'ForzeeStereo'
newDrumkit = 'ForzeeMod'
-- We start by creating a new folder for the modified drumkit
-- If the new folder is already present, remove it.
for ffile in posix.files( '.' ) do
   if string.find( ffile, newDrumkit ) then
      -- Since I'm only able to delete empty folders, delete its
      -- content first.
      for ccontent in posix.files( './' .. newDrumkit ) do
	 posix.unlink( './' .. newDrumkit .. '/' .. ccontent )
      end
      ok = posix.rmdir( newDrumkit )
      if not ok then
	 print( 'Unable to delete obsolete folder ' .. newDrumkit )
      end
   end
end
ok = posix.mkdir( './' .. newDrumkit )
if not ok then
   print( 'Unable to create folder for drumkit ' .. newDrumkit )
   os.exit( ok )
end
-- Since the `applyplugin` command line interface of the LADSPA
-- plugins is not that sophisticated, we have to convert the 24bit WAV
-- files into 16bit WAV ones and downmix the stereo audio files to a
-- single channel (mon. This will be performed by the libsox
-- library. Because we apply both a reduction in the bit depth and a
-- downmixing, SoX does also applies some dithering with noise drawn
-- from a triangular distribution to the resulting audio to cope with
-- quantization distortion. In the downmixing the left and right
-- channel are summed and the ceiling is applied on the result.
for ffile in posix.files( './' .. oldDrumkit ) do
   -- Check whether the file is an .wav file
   if string.find( ffile, '.wav' ) then
      -- Use `sox` to convert the file and save the modified one in
      -- the new folder
      ok = os.execute( 'sox ./' .. oldDrumkit .. '/' .. ffile ..
			  ' --channels 1 --bits 16 --norm=-5 ./' ..
			  newDrumkit .. '/' .. ffile )
      if not ok then
	 print( 'Unable to convert ' .. ffile )
      end
   end

   -- The "drumkit.xml" file must be present in every drumkit. If it
   -- is encountered, copy it to the new folder.
   if string.find( ffile, 'drumkit.xml' ) then
      copyFile( './' .. oldDrumkit .. '/' .. ffile, 
		'./' .. newDrumkit .. '/' .. ffile )
   end
end

--- Plugins and corresponding labels used in the conversion.
-- To use the `applyplugin` function to apply a LADSPA plugin to a
-- sound file, we need to know the name of the shared object the
-- plugin is stored in and the label of the component we want to use.
--
-- The name of the shared object can be obtained by running the
-- function `ladspalist` in a terminal and grepping the output for the
-- desired plugin, e.g. `ladspalist | grep 'Glame Highpass'`. The
-- number at the very beginning of the line can now be used to
-- identify the shared object, e.g. `ls /usr/lib/ladspa | grep 1890`.
--
-- The label can be found in the property 'Plugin Label:' listed when
-- supplying the name of the shared object to the `analyseplugin`
-- function, e.g. `analyseplugin highpass_iir_1890.so`.
--
-- The following function uses some `awk` and `sed` magic to extract
-- the label of a plugin by calling the `analyseplugin` function on
-- the corresponding shared object via the system's command line.
extractLabel = function( sharedObject )
   return io.popen( 'analyseplugin ' .. sharedObject ..
		       ' | grep "Plugin Label:" | awk ' ..
		       "'{print $3}'" ..
		       ' | sed \'s/\\"//g\'' ):read( 'l' )
end
pluginGlameHighpass = 'highpass_iir_1890.so'
pluginGlameHighpassLabel = extractLabel( pluginGlameHighpass )
pluginPlateReverb = 'plate_1423.so'
pluginPlateReverbLabel = extractLabel( pluginPlateReverb )
pluginMultibandEQ = 'mbeq_1197.so'
pluginMultibandEQLabel = extractLabel( pluginMultibandEQ )
pluginTapEQ = 'tap_eqbw.so'
pluginTapEQLabel = extractLabel( pluginTapEQ )

--- Apply the plugins to specific samples
-- Since applying the individual plugins is quite verbose, the
-- procedure is wrapped in a function
applyPlugin = function( fileOld, fileNew, plugin, label, parameters )
   os.execute( 'applyplugin ./' .. newDrumkit ..
		  '/' .. fileOld .. ' ./' .. newDrumkit ..
		  '/' .. fileNew .. ' ' .. plugin .. ' ' .. 
		  label .. ' ' .. parameters )
end

for ffile in posix.files( './' .. newDrumkit ) do

   if string.find( ffile, 'Kick-' ) then
      applyPlugin( ffile, 'tmp.wav', pluginGlameHighpass,
		   pluginGlameHighpassLabel, '67 1' )
      applyPlugin( 'tmp.wav', ffile, pluginMultibandEQ,
		   pluginMultibandEQLabel,
		   '0 0 3 -3 -2 -5 -6 -6 -8 -6 -1 0 0 0 0' )
   end

   if string.find( ffile, 'Snare-' ) then
      applyPlugin( ffile, 'tmp.wav', pluginPlateReverb,
		   pluginPlateReverbLabel, '6.5 0.37 0.35' )
      copyFile( './' .. newDrumkit .. '/tmp.wav',
		'./' .. newDrumkit .. '/' .. ffile )
   end

   if string.find( ffile, 'HiHatSemiopen-' ) then
      applyPlugin( ffile, 'tmp.wav', pluginMultibandEQ,
		   pluginMultibandEQLabel,
		   '-44 -22 -21 -15 -13 -13 -10 -5 -4 -4 -4 0 ' ..
		      '0 -3 -11' )
      copyFile( './' .. newDrumkit .. '/tmp.wav',
		'./' .. newDrumkit .. '/' .. ffile )
   end

   if string.find( ffile, 'RideBow-' ) then
      -- There is an ugly resonance in this file which one has to get
      -- rid of.
      applyPlugin( ffile, 'tmp.wav', pluginTapEQ,
		   pluginTapEQLabel,
		   '-9.5 -38.5 -9.5 -11.5 -8 -9 0 0 196 325 590 ' ..
		      '2660 3450 4500 12000 15000 0.35 0.25 0.4 ' ..
		      '0.2 0.15 0.4 0.1 1' )
      copyFile( './' .. newDrumkit .. '/tmp.wav',
		'./' .. newDrumkit .. '/' .. ffile )
		   
   end

   if string.find( ffile, 'TomLow-' ) then
      applyPlugin( ffile, 'tmp.wav', pluginTapEQ,
		   pluginTapEQLabel,
		   '0 7.5 -8.5 1.5 0.5 0 0 0 100 180 550 1000 3000 ' ..
		      '6000 12000 15000 1 1.2 1.35 1 1 1 1 1' )
      copyFile( './' .. newDrumkit .. '/tmp.wav',
		'./' .. newDrumkit .. '/' .. ffile )
		   
   end
   -- Delete the temporary file
   posix.unlink( './' .. newDrumkit .. '/tmp.wav' )
end
