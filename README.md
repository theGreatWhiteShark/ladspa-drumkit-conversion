In this project I will use and existing drumkit of Hydrogen, the one
called
[ForzeeStereo](https://sourceforge.net/projects/hydrogen/files/Sound%20Libraries/Main%20sound%20libraries/ForzeeStereo.h2drumkit/download),
and apply a couple of LADSPA effects to the individual instruments to
obtain a "better" sounding version. Especially, since sample of the
ride played on its bow produces some nasty resonance, which is
absolutely unbearable.

# Resources / Requirements

The **drumkit** itself is not part of the repository but can be downloaded
using
[here](https://sourceforge.net/projects/hydrogen/files/Sound%20Libraries/Main%20sound%20libraries/ForzeeStereo.h2drumkit/download). 

The **LADSPA** (Linux Audio Developer's Simple Plugin API) plugins can
be installed via the package repositories. In the script I will use
the plugins of [Steve Harris](https://github.com/swh/ladspa) and a
[TAP](http://tap-plugins.sourceforge.net/) one. In addition,
[SoX](http://sox.sourceforge.net/) will be used to convert the drum
samples into a format supported by the LADSPA command-line tool
`applyplugin`. To install all of them on Debian-based systems, enter
the following command

``` bash
sudo apt-get install swh-plugin tap-plugins ladspa-sdk sox
```

The **Lua** script itself is written with Lua5.3 in mind but might
also work with earlier versions. It depends on the
[luaposix](https://github.com/luaposix/luaposix) package, which can be
installed using [LuaRocks](http://www.luarocks.org/)

``` bash
luarocks install luaposix
```

The small **R script** to analyse the downmixing performed by SoX
depends on R3.0.0 or newer and the
[tuneR](https://cran.r-project.org/web/packages/tuneR/) package.

# Run the script

The option `-i` let's you enter the interactive mode after executing
the script.

``` bash
lua -i ladspa_conversion.lua
```

# Note

The specific values for each effect had been adjusted in a live
session for a single sample of the instrument but will be applied to
all samples of the same instrument.
